package dk.dtu.course.dsc.ac.client;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class Tests {

	private final static String data = "_password.xml";
	private final static String[] commands = { "start", "restart", "print testfile testprinter", "queue", "top queue 1",
			"stop", "status", "set config testvalue", "read config testvalue" };

	public static void run(Client client) throws SAXException, IOException, ParserConfigurationException {
		Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new File(data));
		doc.getDocumentElement().normalize();
		doc.getDocumentElement();
		NodeList nl = doc.getElementsByTagName("user");

		for (int i = 0; i < nl.getLength(); i++) {
			Node node = nl.item(i);
			NodeList innerNl = node.getChildNodes();

			String username = null;
			String password = null;

			for (int j = 0; j < innerNl.getLength(); j++) {
				Node innerNode = innerNl.item(j);
				if ("name".equals(innerNode.getNodeName())) {
					username = innerNode.getTextContent();
				} else if ("password".equals(innerNode.getNodeName())) {
					password = innerNode.getTextContent();
				}
			}
			System.out.println("Testing: " + username);
			client.setPasswords(username, password);
			for (String command : commands) {
				System.out.println(command);
				client.parseCommand(command);
			}

		}
	}
}
