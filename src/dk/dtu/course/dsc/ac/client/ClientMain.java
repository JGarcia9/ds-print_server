package dk.dtu.course.dsc.ac.client;

import java.io.IOException;
import java.net.MalformedURLException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.Scanner;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

public class ClientMain {

	public static void main(String[] args) {
		Client client;
		try {
			Scanner input = new Scanner(System.in);
			System.out.println("Write 'test' for the automated testing mode, write 'login' to use it normally.");
			String response = input.nextLine();

			client = new Client();
			if (response.equals("test")) {
				try {
					Tests.run(client);
				} catch (SAXException | IOException | ParserConfigurationException e) {
					e.printStackTrace();
				}
			} else {
				int commRet;
				System.out.println("Please write your details for login the system:");
				do {
					client.askForPassword();
					while ((commRet = client.parseCommand(client.waitForCommand())) > 0)
						;
					if (commRet < 0) {
						System.out.println("The user couldn't be validated, please reenter your login information");
					}
				} while (commRet < 0);
			}
			input.close();
		} catch (MalformedURLException | RemoteException | NotBoundException e) {
			System.out.println("The client couldn't create conenction to the registry.");
			e.printStackTrace();
		}
	}
}
