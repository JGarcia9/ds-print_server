package dk.dtu.course.dsc.ac.client;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.List;
import java.util.Map.Entry;

import dk.dtu.course.dsc.ac.server.common.ServerInterface;

import java.util.Scanner;

public class Client {

	private String name;
	private String pass;
	private Scanner input;
	private byte[] ticket = null;
	private ServerInterface ref;

	public Client() throws MalformedURLException, RemoteException, NotBoundException {
		ref = (ServerInterface) Naming.lookup("rmi://localhost:2300/tag");
		input = new Scanner(System.in);
	}

	public void askForPassword() {
		System.out.println("Name:");
		name = input.nextLine();
		System.out.println("Pasword:");
		pass = input.nextLine();
		ticket = null;
	}

	public void setPasswords(String name, String pass) {
		this.name = name;
		this.pass = pass;
		this.ticket = null;
	}

	public String waitForCommand() {
		System.out.println("Waiting for command:");
		return input.nextLine();
	}

	private void callbackPrint(String[] commands) throws RemoteException {
		if (commands.length != 3) {
			System.out.println("'print <filename> <printer>'");
		} else {
			ticket = ref.print(ticket, commands[1], commands[2]);
		}
	}

	private void callbackQueue(String[] commands) throws RemoteException {
		Entry<byte[], List<Entry<Integer, String>>> queue = ref.queue(ticket);
		if (queue != null) {
			ticket = queue.getKey();
			if (queue.getValue() != null) {
				System.out.format("%20s%20s%n", "JobID", "File");
				System.out.println(String.format("%039d", 0).replace("0", "="));
				for (Entry<Integer, String> entry : queue.getValue()) {
					System.out.format("%20d%20s%n", entry.getKey(), entry.getValue());
				}
				System.out.println(String.format("%039d", 0).replace("0", "="));
			}
		}
	}

	private void callbackTopQueue(String[] commands) throws RemoteException {
		if ((commands.length == 3) && commands[1].equals("queue")) {
			try {
				ticket = ref.topQueue(ticket, new Integer(commands[2]));
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}
		} else {
			System.out.println("'top queue <job>'");
		}
	}

	private void callbackStart(String[] commands) throws RemoteException {
		ticket = ref.start(ticket);
	}

	private void callbackStop(String[] commands) throws RemoteException {
		ticket = ref.start(ticket);
	}

	private void callbackRestart(String[] commands) throws RemoteException {
		ticket = ref.restart(ticket);
	}

	private void callbackStatus(String[] commands) throws RemoteException {
		Entry<byte[], List<Entry<String, String>>> printers = ref.status(ticket);
		if (printers != null) {
			ticket = printers.getKey();
			if (printers.getValue() != null) {
				System.out.format("%25s%25s%n", "Printer", "Status");
				System.out.println(String.format("%049d", 0).replace("0", "="));
				for (Entry<String, String> entry : printers.getValue()) {
					System.out.format("%25s%25s%n", entry.getKey(), entry.getValue());
				}
				System.out.println(String.format("%049d", 0).replace("0", "="));
			}
		}
	}

	private void callbackRead(String[] commands) throws RemoteException {
		if ((commands.length == 3) && commands[1].equals("config")) {
			Entry<byte[], String> parameter = ref.readConfig(ticket, commands[2]);
			if (parameter != null) {
				ticket = parameter.getKey();
				if (parameter.getValue() != null)
					System.out.println(commands[2] + " : " + parameter.getValue());
			} else {
				System.out.println("'read config <paramenter'>'");
			}
		}
	}

	private void callbackSet(String[] commands) throws RemoteException {
		if ((commands.length == 3) && commands[1].equals("config")) {
			ticket = ref.setConfig(ticket, commands[1], commands[2]);
		} else {
			System.out.println("'set config <parameter> <value>'");
		}
	}

	private void callbackLogin(String[] commands) throws RemoteException {
		if (ticket == null) {
			ticket = ref.login(name, pass);
		}
	}

	private void callbackExit(String[] commands) {
		this.name = null;
		this.pass = null;
	}

	private int parseResponse(String command) {
		int returnValue;
		if (ticket == null) {
			System.out.println("You were disconnected by the server.");
			returnValue = -1;
		} else {
			returnValue = 1;
			if (ticket[ticket.length - 1] == 0) {
				System.out.println("The operation " + command + " couldn't be completed -- AccessDenied.");
			} else {
				System.out.println("The operation " + command + " was completed successfully.");
			}
		}
		return returnValue;
	}

	public int parseCommand(String command) {
		String[] commands = command.toLowerCase().split(" ");
		int returnValue = -1;
		try {
			callbackLogin(commands);
			if (this.ticket != null) {
				switch (commands[0]) {
				case "print":
					callbackPrint(commands);
					break;
				case "queue":
					callbackQueue(commands);
					break;
				case "top":
					callbackTopQueue(commands);
					break;
				case "start":
					callbackStart(commands);
					break;
				case "stop":
					callbackStop(commands);
					break;
				case "restart":
					callbackRestart(commands);
					break;
				case "status":
					callbackStatus(commands);
					break;

				case "read":
					callbackRead(commands);
					break;
				case "set":
					callbackSet(commands);
					break;
				case "exit":
					callbackExit(commands);
					returnValue = 0;
					break;
				}
				if (returnValue != 0) {
					returnValue = parseResponse(command);
				}
			}
		} catch (RemoteException e) {
			System.out.println("The server couldn't comply with the request.");
			returnValue = -1;
		}
		return returnValue;
	}

}
