package dk.dtu.course.dsc.ac.server.common;

import java.nio.ByteBuffer;

public class Ticket {
	private long nonce;
	private long userId;
	private byte success;

	public Ticket(long userId, long nonce) {
		this(userId, nonce, true);
	}

	public Ticket(long userId, long nonce, boolean success) {
		this.nonce = nonce;
		this.userId = userId;
		this.success = (byte) ((success) ? 1 : 0);
	}

	public Ticket(byte[] ticket) {
		if (ticket == null) {
			this.nonce = 0;
			this.userId = 0;
		} else {
			ByteBuffer buffer = ByteBuffer.wrap(ticket);
			this.nonce = buffer.getLong();
			// this.timestamp = buffer.getLong();
			this.userId = buffer.getLong();
		}
	}

	public boolean getSuccessful() {
		return (this.success == 0) ? false : true;
	}

	public boolean equals(Ticket ticket) {
		return (this.nonce == ticket.nonce) && (this.userId == ticket.userId);
	}

	public byte[] getBytes() {
		ByteBuffer buffer = ByteBuffer.allocate(Long.BYTES * 2 + 1);
		buffer.putLong(nonce);
		buffer.putLong(userId);
		buffer.put(success);
		return buffer.array();
	}

	public long getNonce() {
		return this.nonce;
	}

	public long getUserId() {
		return this.userId;
	}
}
