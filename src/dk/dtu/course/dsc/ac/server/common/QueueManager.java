package dk.dtu.course.dsc.ac.server.common;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.TimerTask;
import java.util.concurrent.Semaphore;

public class QueueManager extends TimerTask implements List<SimpleEntry<Integer, String>> {

	private LinkedList<SimpleEntry<Integer, String>> queue;
	private Semaphore sem;
	private String printer;

	private enum Statuses {
		IDLE("Idle"), PRINTING("Printing"), DISCONNECTED("Disconnected"), ERROR("Error");

		private String name;

		private Statuses(String name) {
			this.name = name;
		}

		public String toString() {
			return name;
		}
	};

	private Statuses status;

	public QueueManager(String printer) {
		this.queue = new LinkedList<SimpleEntry<Integer, String>>();
		this.sem = new Semaphore(1);
		this.printer = printer;
		File printerFile = new File(printer);
		try {
			printerFile.createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
		this.status = Statuses.IDLE;
	}

	public String getName() {
		return printer;
	}

	@Override
	public void run() {
		try {
			this.sem.acquire();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		if (!queue.isEmpty()) {
			SimpleEntry<Integer, String> toPrinterString = this.queue.removeFirst();
			if (queue.isEmpty()) {
				this.status = Statuses.IDLE;
			}
			PrintWriter writer = null;
			;
			try {
				writer = new PrintWriter(new FileWriter(new File(this.printer), true));
				writer.write(toPrinterString.getValue());
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			writer.close();

		} else {
			this.status = Statuses.IDLE;
		}
		this.sem.release();
	}

	@Override
	public boolean add(SimpleEntry<Integer, String> e) {
		try {
			this.sem.acquire();
		} catch (InterruptedException except) {
			except.printStackTrace();
		}
		boolean retVal = this.queue.add(e);
		this.status = Statuses.PRINTING;
		this.sem.release();
		return retVal;
	}

	public SimpleEntry<Integer, String> find(Integer jobnum) {
		for (SimpleEntry<Integer, String> smpl : queue) {
			if (smpl.getKey() == jobnum) {
				return smpl;
			}
		}
		return null;
	}

	@Override
	public void add(int index, SimpleEntry<Integer, String> element) {
		try {
			this.sem.acquire();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		this.status = Statuses.PRINTING;
		this.queue.add(index, element);
		this.sem.release();
	}

	@Override
	public boolean addAll(Collection<? extends SimpleEntry<Integer, String>> c) {
		try {
			this.sem.acquire();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		this.status = Statuses.PRINTING;
		boolean retVal = this.queue.addAll(c);
		this.sem.release();
		return retVal;
	}

	@Override
	public boolean addAll(int index, Collection<? extends SimpleEntry<Integer, String>> c) {
		try {
			this.sem.acquire();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		this.status = Statuses.PRINTING;
		boolean retVal = this.queue.addAll(index, c);
		this.sem.release();
		return retVal;
	}

	@Override
	public void clear() {
		try {
			this.sem.acquire();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		this.status = Statuses.IDLE;
		this.queue.clear();
		this.sem.release();
	}

	@Override
	public boolean contains(Object o) {
		try {
			this.sem.acquire();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		boolean retVal = this.queue.contains(o);
		this.sem.release();
		return retVal;
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		try {
			this.sem.acquire();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		boolean retVal = this.queue.containsAll(c);
		this.sem.release();
		return retVal;
	}

	@Override
	public SimpleEntry<Integer, String> get(int index) {
		try {
			this.sem.acquire();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		SimpleEntry<Integer, String> retVal = this.queue.get(index);
		this.sem.release();
		return retVal;
	}

	@Override
	public int indexOf(Object o) {
		try {
			this.sem.acquire();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		int retVal = this.queue.indexOf(o);
		this.sem.release();
		return retVal;
	}

	@Override
	public boolean isEmpty() {
		try {
			this.sem.acquire();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		boolean retVal = this.queue.isEmpty();
		this.sem.release();
		return retVal;
	}

	@Override
	public Iterator<SimpleEntry<Integer, String>> iterator() {
		try {
			this.sem.acquire();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		Iterator<SimpleEntry<Integer, String>> retVal = this.queue.iterator();
		this.sem.release();
		return retVal;
	}

	@Override
	public int lastIndexOf(Object o) {
		try {
			this.sem.acquire();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		int retVal = this.queue.lastIndexOf(o);
		this.sem.release();
		return retVal;
	}

	@Override
	public ListIterator<SimpleEntry<Integer, String>> listIterator() {
		try {
			this.sem.acquire();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		ListIterator<SimpleEntry<Integer, String>> retVal = this.queue.listIterator();
		this.sem.release();
		return retVal;
	}

	@Override
	public ListIterator<SimpleEntry<Integer, String>> listIterator(int index) {
		try {
			this.sem.acquire();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		ListIterator<SimpleEntry<Integer, String>> retVal = this.queue.listIterator(index);
		this.sem.release();
		return retVal;
	}

	@Override
	public boolean remove(Object o) {
		try {
			this.sem.acquire();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		boolean retVal = this.queue.remove(o);
		this.sem.release();
		return retVal;
	}

	@Override
	public SimpleEntry<Integer, String> remove(int index) {
		try {
			this.sem.acquire();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		SimpleEntry<Integer, String> retVal = this.queue.remove(index);
		this.sem.release();
		return retVal;
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		try {
			this.sem.acquire();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		boolean retVal = this.queue.removeAll(c);
		this.sem.release();
		return retVal;
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		try {
			this.sem.acquire();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		boolean retVal = this.queue.retainAll(c);
		this.sem.release();
		return retVal;
	}

	@Override
	public SimpleEntry<Integer, String> set(int index, SimpleEntry<Integer, String> element) {
		try {
			this.sem.acquire();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		SimpleEntry<Integer, String> retVal = this.queue.set(index, element);
		this.sem.release();
		return retVal;
	}

	@Override
	public int size() {
		try {
			this.sem.acquire();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		int retVal = this.queue.size();
		this.sem.release();
		return retVal;
	}

	@Override
	public List<SimpleEntry<Integer, String>> subList(int fromIndex, int toIndex) {
		try {
			this.sem.acquire();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		List<SimpleEntry<Integer, String>> retVal = this.queue.subList(fromIndex, toIndex);
		this.sem.release();
		return retVal;
	}

	@Override
	public Object[] toArray() {
		try {
			this.sem.acquire();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		Object[] retVal = this.queue.toArray();
		this.sem.release();
		return retVal;
	}

	@Override
	public <T> T[] toArray(T[] a) {
		try {
			this.sem.acquire();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		T[] retVal = this.queue.toArray(a);
		this.sem.release();
		return retVal;
	}

	public void addFirst(SimpleEntry<Integer, String> e) {
		this.status = Statuses.PRINTING;
		this.queue.addFirst(e);
	}

	public ArrayList<SimpleEntry<Integer, String>> copyList() {
		return new ArrayList<SimpleEntry<Integer, String>>(this.queue);
	}

	public LinkedList<SimpleEntry<Integer, String>> getList() {
		return this.queue;
	}

	public String getStatus() {
		return status.toString();
	}
}
