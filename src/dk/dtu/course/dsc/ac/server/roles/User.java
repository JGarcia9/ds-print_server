package dk.dtu.course.dsc.ac.server.roles;

import dk.dtu.course.dsc.ac.server.common.Ticket;

public class User {

	private long userId;
	private Ticket ticket;
	private String username;
	private String role;
	private long lastAction;

	public User(String username, long userId, String role) {
		this.userId = userId;
		this.role = role;
		this.setUsername(username);
		this.setTicket(null);
		this.lastAction = System.currentTimeMillis();
	}

	public User(String username, long userId) {
		this(username, userId, "");
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public Ticket getTicket() {
		return ticket;
	}

	public void setTicket(Ticket ticket) {
		this.ticket = ticket;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void refreshLastActionTime() {
		this.lastAction = System.currentTimeMillis();
	}

	public long getLastActionTime() {
		return lastAction;
	}

	public String getRole() {
		return this.role;
	}
}
