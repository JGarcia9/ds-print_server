package dk.dtu.course.dsc.ac.server.roles;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.SignatureException;
import java.security.spec.X509EncodedKeySpec;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Timer;
import java.util.TreeMap;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import dk.dtu.course.dsc.ac.server.common.CryptoHash;
import dk.dtu.course.dsc.ac.server.common.QueueManager;
import dk.dtu.course.dsc.ac.server.common.ServerInterface;
import dk.dtu.course.dsc.ac.server.common.Ticket;

public class PrintingServer extends UnicastRemoteObject implements ServerInterface {

	private static final long serialVersionUID = -169897565899774133L;
	private static final long ticketValidityTime = 120000;
	private static final long printerActionTime = 1000 * 10;
	private static final String passwordFile = "_data_role.bin";
	private static final String roleFile = "_role.xml";
	private static final String signatureFile = "_sign.bin";
	private static final String publicKeyFile = "_printer.pub";
	private static final String logFile = "logfile.txt";
	private static final int PPRINT = 0b000000001;
	private static final int PQUEUE = 0b000000010;
	private static final int PTOPQUEUE = 0b000000100;
	private static final int PSTART = 0b000001000;
	private static final int PRESTART = 0b000010000;
	private static final int PSTOP = 0b000010000;
	private static final int PSTATUS = 0b000100000;
	private static final int PREADCONFIG = 0b001000000;
	private static final int PSETCONFIG = 0b010000000;
	private static final int PALL = 0b011111111;

	private boolean isStarted = false;
	private BufferedWriter logwriter;
	private PublicKey publKey;
	private CryptoHash hasher;
	private Timer timer;
	// private HashMap<Long, Ticket> activeTickets;
	private HashMap<Long, User> activeUsers;
	private TreeMap<String, QueueManager> printers;
	private HashMap<String, String> parameters;
	private HashMap<String, Integer> roles;

	/**
	 * Constructor of PrintingSever.
	 * 
	 * @throws RemoteException
	 */
	public PrintingServer() throws RemoteException {
		super();
		try {
			hasher = new CryptoHash("SHA-256");
			Path path = Paths.get(PrintingServer.publicKeyFile);
			byte[] publKeyBytes = Files.readAllBytes(path);
			KeyFactory keyFactory = KeyFactory.getInstance("RSA");
			X509EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(publKeyBytes);
			publKey = keyFactory.generatePublic(publicKeySpec);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			throw new RemoteException("Cannot start the server.");
		}
		this.printers = new TreeMap<String, QueueManager>();
		this.activeUsers = new HashMap<Long, User>();
		this.parameters = new HashMap<String, String>();
		this.roles = readRoles();
		this.timer = new Timer();
	}

	private HashMap<String, Integer> readRoles() throws RemoteException {
		HashMap<String, Integer> returnBuffer = new HashMap<String, Integer>();
		Document doc;
		try {
			doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new File(roleFile));
		} catch (SAXException | IOException | ParserConfigurationException e) {
			e.printStackTrace();
			throw new RemoteException(e.getMessage(), e.getCause());
		}
		doc.getDocumentElement().normalize();
		NodeList nl = doc.getElementsByTagName("role");

		for (int i = 0; i < nl.getLength(); i++) {
			Node node = nl.item(i);
			NodeList innerNl = node.getChildNodes();

			String rolename = null;
			int permissions = 0;

			for (int j = 0; j < innerNl.getLength(); j++) {
				Node innerNode = innerNl.item(j);
				if ("rolename".equals(innerNode.getNodeName())) {
					rolename = innerNode.getTextContent();
				} else if ("permissions".equals(innerNode.getNodeName())) {
					permissions = permissions | parsePermission(innerNode.getTextContent());
				}
			}
			returnBuffer.put(rolename, permissions);
		}

		return returnBuffer;
	}

	/**
	 * Searches for the user in the password file. If it finds it AND the password
	 * matches, it will return the user name. Otherwise it will return null;
	 * 
	 * @param username
	 *            The user name to check.
	 * @param password
	 *            The password to check.
	 * @return The found user, or null if it didn't find it.
	 * @throws RemoteException
	 */
	private User getUserFromPassFile(String username, String password) throws RemoteException {
		User newUser = null;
		Entry<byte[], String> readPass = getFromFile(username, passwordFile, signatureFile,
				32 + hasher.getSaltLenght());
		if (readPass != null) {
			if (hasher.compareHash(password, readPass.getKey())) {
				long userId = hasher.getRandLong();
				while (activeUsers.containsKey(userId)) {
					userId = hasher.getRandLong();
				}
				newUser = new User(username, userId, readPass.getValue());
			}
		}
		return newUser;
	}

	/**
	 * Searches in either the passwordFile or in the policyFile for the user name
	 * and returns the password hash in the first case, and the permission bytes in
	 * the second.
	 * 
	 * @param username
	 *            The user name to find.
	 * @param type
	 *            The string "policy" or "password".
	 * @return Bytes of either the password hash or the permissions. Or null if they
	 *         couldn't be found.
	 * @throws RemoteException
	 */
	private Entry<byte[], String> getFromFile(String username, String filename, String signFile, int bufferLenght)
			throws RemoteException {
		Entry<byte[], String> returnBuffer = null;
		try {
			if (hasher.checkFileSignature(filename, signFile, this.publKey)) {
				FileInputStream fin = new FileInputStream(new File(filename));
				int strlen;
				while ((strlen = fin.read()) > -1) {
					byte[] nameBuffer = new byte[strlen];
					fin.read(nameBuffer);
					String readUser = new String(nameBuffer);
					strlen = fin.read();
					byte[] permBuffer = new byte[strlen];
					fin.read(permBuffer);
					String readPerms = new String(permBuffer);
					byte[] nextBuffer = new byte[bufferLenght];
					fin.read(nextBuffer);
					if (readUser.equals(username)) {
						returnBuffer = new SimpleEntry<byte[], String>(nextBuffer, readPerms);
						break;
					}
				}
				fin.close();
			}
		} catch (InvalidKeyException | SignatureException | NoSuchAlgorithmException | IOException e) {
			e.printStackTrace();
			throw new RemoteException(e.getMessage(), e.getCause());
		}
		return returnBuffer;
	}

	@Override
	public byte[] login(String username, String password) throws RemoteException {
		byte[] ticket = null;
		User user = getUserFromPassFile(username, password);
		if (user != null) {
			Ticket tick = new Ticket(user.getUserId(), hasher.getRandLong());
			user.setTicket(tick);
			if (isStarted)
				try {
					this.logwriter.write("Login of user '" + user.getUsername() + "' on " + user.getLastActionTime()
							+ "ms Assigned ID: " + user.getUserId() + ".\n");
				} catch (IOException e) {
					e.printStackTrace();
					throw new RemoteException(e.getMessage(), e.getCause());
				}
			this.activeUsers.put(user.getUserId(), user);
			ticket = user.getTicket().getBytes();
		} else {
			try {
				if (isStarted)
					this.logwriter.write("User '" + username + "' tried to connect with an incorrect password on "
							+ System.currentTimeMillis() + " ms.\n");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return ticket;
	}

	@Override
	public byte[] print(byte[] ticket, String filename, String printer) throws RemoteException {
		try {
			if (!this.isStarted) {
				return null;
			}
			Ticket parsedTicket = new Ticket(ticket);
			boolean success = false;
			User user;
			if ((user = checkTicket(parsedTicket)) != null) {
				if ((filename.charAt(0) == '_') || filename.equals(PrintingServer.passwordFile)
						|| filename.equals(PrintingServer.publicKeyFile)
						|| filename.equals(PrintingServer.signatureFile)) {
					return null;
				}
				if (checkPermission("print", user)) {
					if (!this.printers.containsKey(printer)) {
						QueueManager qm = new QueueManager(printer);
						timer.scheduleAtFixedRate(qm, PrintingServer.printerActionTime,
								PrintingServer.printerActionTime);
						printers.put(printer, qm);
					}
					printers.get(printer)
							.add(new SimpleEntry<Integer, String>(printers.get(printer).size() + 1, filename));
					success = true;
					this.logwriter.write("Print of user '" + user.getUsername() + "' on printer '" + printer
							+ "' of  file " + filename + " on " + System.currentTimeMillis() + "ms.\n");
				} else {
					this.logwriter
							.write("User " + user.getUsername() + " denied access to the operation 'print' on time "
									+ System.currentTimeMillis() + "ms.\n");
				}
				byte[] newTicket = this.refreshTicket(parsedTicket, success).getBytes();
				return newTicket;
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new RemoteException(e.getMessage(), e.getCause());
		}
		return null;
	}

	private int parsePermission(String permission) {
		switch (permission) {
		case "print":
			return PPRINT;
		case "queue":
			return PQUEUE;
		case "topqueue":
			return PTOPQUEUE;
		case "start":
			return PSTART;
		case "stop":
			return PSTOP;
		case "restart":
			return PRESTART;
		case "status":
			return PSTATUS;
		case "readconfig":
			return PREADCONFIG;
		case "setconfig":
			return PSETCONFIG;
		case "all":
			return PALL;
		default:
			return 0;
		}
	}

	private boolean checkPermission(String permission, User user) {

		int perms = this.roles.get(user.getRole());
		switch (permission) {
		case "print":
			return ((PPRINT & perms) != 0);
		case "queue":
			return ((PQUEUE & perms) != 0);
		case "topqueue":
			return ((PTOPQUEUE & perms) != 0);
		case "start":
			return ((PSTART & perms) != 0);
		case "stop":
			return ((PSTOP & perms) != 0);
		case "restart":
			return ((PRESTART & perms) != 0);
		case "status":
			return ((PSTATUS & perms) != 0);
		case "readconfig":
			return ((PREADCONFIG & perms) != 0);
		case "setconfig":
			return ((PSETCONFIG & perms) != 0);
		default:
			return false;
		}
	}

	@Override
	public Entry<byte[], List<Entry<Integer, String>>> queue(byte[] ticket) throws RemoteException {
		try {
			if (!this.isStarted) {
				return null;
			}
			Ticket parsedTicket = new Ticket(ticket);
			User user;
			if ((user = checkTicket(parsedTicket)) != null) {
				Entry<byte[], List<Entry<Integer, String>>> retVal;
				if (checkPermission("queue", user)) {
					List<Entry<Integer, String>> list = new LinkedList<>();
					for (QueueManager qm : printers.values()) {
						list.addAll(qm.getList());
					}
					retVal = new SimpleEntry<>(this.refreshTicket(parsedTicket, true).getBytes(), list);
					this.logwriter.write("Queue request of user '" + user.getUsername() + "' on "
							+ System.currentTimeMillis() + "ms.\n");
				} else {
					retVal = new SimpleEntry<>(this.refreshTicket(parsedTicket, false).getBytes(), null);
					this.logwriter.write("User '" + user.getUsername() + "' denied access to the operation 'queue' on "
							+ System.currentTimeMillis() + "ms.\n");
				}
				return retVal;
			}
		} catch (Exception e) {
			throw new RemoteException(e.getMessage(), e.getCause());
		}
		return null;
	}

	@Override
	public byte[] topQueue(byte[] ticket, int job) throws RemoteException {
		try {
			if (!this.isStarted) {
				return null;
			}
			Ticket parsedTicket = new Ticket(ticket);
			User user;
			if ((user = checkTicket(parsedTicket)) != null) {
				if (checkPermission("topqueue", user)) {
					SimpleEntry<Integer, String> val;
					for (QueueManager col : printers.values()) {
						if ((val = col.find(job)) != null) {
							col.remove(val);
							col.addFirst(val);
							this.logwriter.write("topQueue of user '" + user.getUsername() + "' of job " + val.getKey()
									+ " file " + val.getValue() + " on " + System.currentTimeMillis() + "ms.\n");
							return this.refreshTicket(parsedTicket, true).getBytes();
						}
					}
				} else {
					this.logwriter.write(
							"User '" + user.getUsername() + "' was denied access to the operation 'topQueue'  on "
									+ System.currentTimeMillis() + "ms.\n");
					return this.refreshTicket(parsedTicket, false).getBytes();
				}
			}
		} catch (Exception e) {
			throw new RemoteException();
		}
		return null;
	}

	@Override
	public byte[] start(byte[] ticket) throws RemoteException {
		try {
			Ticket parsedTicket = new Ticket(ticket);
			User user;
			if ((user = checkTicket(parsedTicket)) != null) {
				if (checkPermission("start", user)) {
					this.isStarted = true;
					this.logwriter = new BufferedWriter(new FileWriter(new File(PrintingServer.logFile), true));
					this.logwriter.write("Log started on: " + System.currentTimeMillis() + " by " + user.getUsername());
					return this.refreshTicket(parsedTicket, true).getBytes();
				} else {
					this.logwriter
							.write("User '" + user.getUsername() + "' was denied access to the operation 'start'  on "
									+ System.currentTimeMillis() + "ms.\n");
					return this.refreshTicket(parsedTicket, false).getBytes();
				}
			}
		} catch (Exception e) {
			throw new RemoteException();
		}
		return null;
	}

	@Override
	public byte[] stop(byte[] ticket) throws RemoteException {
		try {
			Ticket parsedTicket = new Ticket(ticket);
			User user;
			if ((user = checkTicket(parsedTicket)) != null) {
				if (checkPermission("stop", user)) {
					this.isStarted = false;
					this.logwriter.write("Server stopped by '" + user.getUsername() + "' on  "
							+ System.currentTimeMillis() + "ms.\n");
					logwriter.close();
					timer.cancel();
					return this.refreshTicket(parsedTicket, true).getBytes();
				} else {
					this.logwriter
							.write("User '" + user.getUsername() + "' was denied access to the operation 'stop'  on "
									+ System.currentTimeMillis() + "ms.\n");
					return this.refreshTicket(parsedTicket, false).getBytes();
				}
			}
		} catch (Exception e) {
			throw new RemoteException();
		}
		return null;
	}

	@Override
	public byte[] restart(byte[] ticket) throws RemoteException {
		try {
			Ticket parsedTicket = new Ticket(ticket);
			User user;
			if ((user = checkTicket(parsedTicket)) != null) {
				if (checkPermission("restart", user)) {
					if (this.isStarted) {
						for (QueueManager qm : this.printers.values()) {
							qm.clear();
						}
						this.printers.clear();
						this.logwriter.write("Server restarted by user '" + user.getUsername() + "' on  "
								+ System.currentTimeMillis() + "ms.\n");
						logwriter.close();
					}
					this.isStarted = true;
					this.logwriter = new BufferedWriter(new FileWriter(new File(PrintingServer.logFile), true));
					this.logwriter.write("Log started on: " + System.currentTimeMillis() + "ms.\n");
					return this.refreshTicket(parsedTicket, true).getBytes();
				} else {
					this.logwriter
							.write("User '" + user.getUsername() + "' was denied access to the operation 'restart'  on "
									+ System.currentTimeMillis() + "ms.\n");
					return this.refreshTicket(parsedTicket, false).getBytes();
				}
			}
		} catch (Exception e) {
			throw new RemoteException();
		}
		return null;
	}

	@Override
	public Entry<byte[], List<Entry<String, String>>> status(byte[] ticket) throws RemoteException {
		try {
			if (!this.isStarted) {
				return null;
			}
			Ticket parsedTicket = new Ticket(ticket);
			User user;
			if ((user = checkTicket(parsedTicket)) != null)
				if (!checkPermission("status", user)) {
					this.logwriter
							.write("User '" + user.getUsername() + "' was denied access to the operation 'status'  on "
									+ System.currentTimeMillis() + "ms.\n");
					return new SimpleEntry<>(this.refreshTicket(parsedTicket, false).getBytes(), null);
				} else {
					List<Entry<String, String>> statusList = new ArrayList<>(this.printers.size());
					for (QueueManager qm : this.printers.values()) {
						statusList.add(new SimpleEntry<>(qm.getName(), qm.getStatus()));
					}
					this.logwriter.write("Status request by user '" + user.getUsername() + "' on "
							+ System.currentTimeMillis() + "ms.\n");
					return new SimpleEntry<>(this.refreshTicket(parsedTicket, true).getBytes(), statusList);
				}
		} catch (Exception e) {
			throw new RemoteException();
		}
		return null;
	}

	@Override
	public Entry<byte[], String> readConfig(byte[] ticket, String parameter) throws RemoteException {
		try {
			if (!this.isStarted) {
				return null;
			}
			Ticket parsedTicket = new Ticket(ticket);
			User user;
			if ((user = checkTicket(parsedTicket)) != null)
				if (!checkPermission("readconfig", user)) {
					this.logwriter.write(
							"User '" + user.getUsername() + "' was denied access to the operation 'readConfig'  on "
									+ System.currentTimeMillis() + "ms.\n");
					return new SimpleEntry<>(this.refreshTicket(parsedTicket, false).getBytes(), null);
				} else {
					this.logwriter.write("ReadConfig request by user '" + user.getUsername() + "' of parameter"
							+ parameter + " on " + System.currentTimeMillis() + "ms.\n");
					return new SimpleEntry<>(this.refreshTicket(parsedTicket, true).getBytes(),
							this.parameters.get(parameter));
				}
		} catch (Exception e) {
			throw new RemoteException();
		}
		return null;
	}

	@Override
	public byte[] setConfig(byte[] ticket, String parameter, String value) throws RemoteException {
		try {
			if (!this.isStarted) {
				return null;
			}
			Ticket parsedTicket = new Ticket(ticket);
			User user;
			if ((user = checkTicket(parsedTicket)) != null)
				if (checkPermission("setconfig", user)) {
					this.logwriter.write(
							"User '" + user.getUsername() + "' was denied access to the operation 'setConfig'  on "
									+ System.currentTimeMillis() + "ms.\n");
					return this.refreshTicket(parsedTicket, false).getBytes();
				} else {
					String oldPram = this.parameters.get(parameter);
					this.parameters.put(parameter, value);
					this.logwriter.write("SetConfig request by user '" + user.getUsername() + "' of parameter"
							+ parameter + " from  " + oldPram + " to " + value + " on " + System.currentTimeMillis()
							+ "ms.\n");
					return this.refreshTicket(parsedTicket, true).getBytes();
				}
		} catch (Exception e) {
			throw new RemoteException();
		}
		return null;
	}

	private User checkTicket(Ticket ticket) {
		User user = this.activeUsers.get(ticket.getUserId());
		if (user != null) {
			if ((user.getLastActionTime() + PrintingServer.ticketValidityTime) > System.currentTimeMillis()) {
				if (user.getTicket().getNonce() == ticket.getNonce()) {
					return user;
				}
			} else {
				this.activeUsers.remove(user.getUserId());
			}
		}
		return null;
	}

	private Ticket refreshTicket(Ticket ticket, boolean success) {
		Ticket refrTicket = null;
		User user = this.activeUsers.get(ticket.getUserId());
		if (user != null) {
			refrTicket = new Ticket(user.getUserId(), this.hasher.getRandLong(), success);
			user.setTicket(refrTicket);
			user.refreshLastActionTime();
		}
		return refrTicket;
	}

}
