package dk.dtu.course.dsc.ac.server.perms;

import dk.dtu.course.dsc.ac.server.common.Ticket;

public class User {
	private long userId;
	private Ticket ticket;
	private String username;
	private long lastAction;

	public User(String username, long userId) {
		this.userId = userId;
		this.setUsername(username);
		this.setTicket(null);
		this.lastAction = System.currentTimeMillis();
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public Ticket getTicket() {
		return ticket;
	}

	public void setTicket(Ticket ticket) {
		this.ticket = ticket;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void refreshLastActionTime() {
		this.lastAction = System.currentTimeMillis();
	}

	public long getLastActionTime() {
		return lastAction;
	}
}
