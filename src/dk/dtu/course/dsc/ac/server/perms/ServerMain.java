package dk.dtu.course.dsc.ac.server.perms;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.rmi.registry.Registry;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.SignatureException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import dk.dtu.course.dsc.ac.server.common.CryptoHash;

public class ServerMain {

	public static void main(String[] args) {
		try {
			ServerMain.populate();
			PrintingServer s = new PrintingServer();
			Registry re = java.rmi.registry.LocateRegistry.createRegistry(2300);
			re.rebind("tag", s);
			System.out.println("PrintingServer class is binded with Registry");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static void populate() throws IOException, NoSuchAlgorithmException, InvalidKeyException,
			SignatureException, KeyStoreException, ParserConfigurationException, SAXException {
		final String filename = "_password.xml";
		final String passwordFile = "_data.bin";
		//final String policyFile = "_plc.bin";
		final String signFile = "_sign.bin";
		//final String polSignFile = "_polsign.bin";
		CryptoHash hasher = new CryptoHash("SHA-256");
		KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA");
		generator.initialize(2048, new SecureRandom());
		KeyPair pair = generator.generateKeyPair();
		createPassFile(hasher, filename, passwordFile, null);
		hasher.signFile(passwordFile, signFile, pair.getPrivate());
		// hasher.signFile(policyFile, polSignFile, pair.getPrivate());
		storePass(pair);
	}

	private static void createPassFile(CryptoHash hasher, String filename, String passwordFile, String policyFile)
			throws NoSuchAlgorithmException, InvalidKeyException, SignatureException, IOException,
			ParserConfigurationException, SAXException {

		Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new File(filename));
		doc.getDocumentElement().normalize();

		File psf = new File(passwordFile);
		if (psf.exists()) {
			psf.delete();
			psf.createNewFile();
		}
		FileOutputStream writer = new FileOutputStream(psf);

		doc.getDocumentElement();
		NodeList nl = doc.getElementsByTagName("user");

		for (int i = 0; i < nl.getLength(); i++) {
			Node node = nl.item(i);
			NodeList innerNl = node.getChildNodes();

			String username = null;
			String password = null;
			// int permissions = 0;

			for (int j = 0; j < innerNl.getLength(); j++) {
				Node innerNode = innerNl.item(j);
				if ("name".equals(innerNode.getNodeName())) {
					username = innerNode.getTextContent();
				} else if ("password".equals(innerNode.getNodeName())) {
					password = innerNode.getTextContent();
				}
			}

			writer.write(username.length());
			writer.write(username.getBytes());
			writer.write(hasher.createHash(password));
			// policyWriter.write(username.length());
			// policyWriter.write(username.getBytes());
			// byte[] perms =
			// ByteBuffer.allocate(Integer.BYTES).putInt(permissions).array();
			// policyWriter.write(perms);
		}
		writer.close();
	}

	private static void storePass(KeyPair pair) throws KeyStoreException, IOException {
		File fl = new File("_printer.pub");
		fl.delete();
		fl.createNewFile();
		FileOutputStream writer = new FileOutputStream(fl);
		writer.write(pair.getPublic().getEncoded());
		writer.close();
	}
}
